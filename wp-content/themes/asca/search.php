<?php
/**
 * The template for displaying Search Results pages
 *
 * @subpackage Twenty_Fourteen
 */
 ?>
<?php require_once(dirname(__FILE__) . '/_layouts/_partials/nav.php'); ?>

    <section class="m-single">
        <div class="m-single__post">

            <?php if ( have_posts() ) : ?>

                <header class="page-header">
                    <h1 class="page-title"><?php printf( __( 'Search Results for: %s'), get_search_query() ); ?></h1>
                </header><!-- .page-header -->

                <?php
                // Start the Loop.
                while ( have_posts() ) : the_post(); ?>

                    <h1> <?php the_title(); ?> </h1>
                    <?php the_excerpt(); ?>

                <?php endwhile; ?>


            <?php else : ?>
                <p><?php _e( 'No Blog Posts found', 'nd_dosth' ); ?></p>

            <?php endif; ?>

        </div><!-- #content -->
        <div class="m-single__sidebar" >
            <?php
            get_sidebar( 'content' );
            get_sidebar(); ?>
        </div>
    </section>

<?php
get_header();
get_footer();
