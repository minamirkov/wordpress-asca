<?php
/* Template Name: Main Page */
?>

<?php $page_slug = 'home';
require_once(dirname(__FILE__) . '/_includes/_head/head_meta.php'); ?>

<body class="page-<?php echo $page_slug; ?>">
    <div id="page-content" class="page-content">
        <?php require_once(dirname(__FILE__) . '/_layouts/_partials/nav.php'); ?>
        <?php require_once(dirname(__FILE__) . '/_layouts/_sections/_hero/index.php'); ?>
        <?php require_once(dirname(__FILE__) . '/_layouts/_sections/_gallery/index.php'); ?>
        <?php require_once(dirname(__FILE__) . '/_layouts/_sections/_panel/index.php'); ?>
        <?php require_once(dirname(__FILE__) . '/_layouts/_sections/_posts/index.php'); ?>
        <?php require_once(dirname(__FILE__) . '/_layouts/_sections/_video-box/index.php'); ?>
        <?php require_once(dirname(__FILE__) . '/_includes/_footer/footer.php'); ?>
    </div>
</body>