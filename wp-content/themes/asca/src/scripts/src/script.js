/* eslint-disable no-console */
document.addEventListener('DOMContentLoaded', function () {
    const menuItems = document.querySelectorAll('.menuItem');
    const hamburger = document.querySelector('.hamburger');
    const _body = document.querySelector('body');

    function toggleMenu() {
        _body.classList.toggle('showMenu');
    }

    hamburger.addEventListener('click', toggleMenu);

    menuItems.forEach(function (menuItem) {
        menuItem.addEventListener('click', toggleMenu);
    });

    const fraction = document.getElementById('fraction');
    const fraction1 = document.getElementById('fraction1');
    const slides = document.querySelectorAll('.m-hero');
    const slideCount = slides.length;
    fraction.textContent = `01 / 0${slideCount}`;
    fraction1.textContent = '01 / 02';


    const swiper = new Swiper('.mySwiper', {
        keyboard: {
            enabled: true,
        },
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true,
        },

        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        on: {
            slideChange: () => {
                fraction.textContent = `0${swiper.realIndex + 1} / 0${slideCount}`;

                if (swiper.realIndex + 2 > slideCount) {
                    fraction1.textContent = `0${swiper.realIndex} / 0${slideCount}`;
                } else {
                    fraction1.textContent = `0${swiper.realIndex} / 0${swiper.realIndex + 2}`;
                }

                if (swiper.realIndex < 1) {
                    fraction1.textContent = ` 01 / 0${swiper.realIndex + 2}`;
                }
            }
        },
    });

    const meni = ['TREES', 'MOUNTAINS', 'JAPAN', 'SEA LEVELS', 'TREES', 'MOUNTAINS'];
    const swiperGallery = new Swiper('.gallerySwiper', {
        slidesPerView: 4,
        spaceBetween: 32,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            renderBullet: function (index, className) {
                return '<span class="' + className + '">' + (meni[index]) + '</span>';
            },
        },
        breakpoints: {
            480: {
                slidesPerView: 1,
                spaceBetween: 15,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            992: {
                slidesPerView: 3,
                spaceBetween: 32,
            },
            1344: {
                slidesPerView: 4,
                spaceBetween: 32,
            },
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

});

