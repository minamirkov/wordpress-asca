<!--samo predlog umesto __right, __left -> __search; __logo; __menu-->
<?php
$site_logo = get_site_icon_url();
?>
<nav>
    <div class="m-nav">
        <div class="m-nav__logo">
            <a href="http://localhost:8080/"> <img src="<?php echo $site_logo; ?>" alt="logo"> </a>
        </div>
        <?php wp_nav_menu(array(
            'container' => 'm-nav',
            'container_class' => 'm-nav',
            'menu_class' => 'm-nav__menu',
        ));
        ?>
        <div class="m-nav__search">
            <!-- <?php get_search_form() ?> -->
            <a href="http://localhost:8080/searchform" class="search">
                <img src="<?php bloginfo('template_directory');?>/src/images/_icons/search%20(2)@2x.png" alt="search">
            </a>
            <a href=""> EN </a>
        </div>
    </div>
</nav>

       <?php wp_nav_menu(array(
           'container' => 'li',
           'container_class' => 'menu',
           'menu_class' => 'menu',
       ));
       ?>

<button id="hamburger" class="hamburger">
    <i class="menuIcon material-icons">menu</i>
    <i class="closeIcon material-icons">close</i>
</button>
