<!--dati nazive sekcijama tako da se mogu koristiti na razlicitim mestima, ukoliko se pojave iste sekcije
    ovako mozda neces moci da se setis sta ti je section1 npr. ovo bi moglo da bude m-slider ili m-hero -->
<!-- Advanced Custom Fields -->
<?php
$hero_feature_image     =   get_field('hero_feature_image');
$hero_section_title     =   get_field('hero_section_title');
$hero_short_description =   get_field('hero_short_description');
$hero_long_des =   get_field('hero_long_description');
$hero_feature_image1     =   get_field('hero_feature_image_1');
$hero_feature_image2     =   get_field('hero_feature_image_2');
$hero_feature_image3     =   get_field('hero_feature_image_3');

?>

<div class="swiper mySwiper">
    <div class="swiper-wrapper">
        <div class="swiper-slide">
            <div class="m-hero" id="about-us">
                <div class="m-hero__left">
                    <p class="m-hero__left--p2"> <?php echo $hero_short_description; ?> <?php bloginfo('name'); ?> </>
                    <h1> <?php echo $hero_section_title; ?> </h1>
                    <p class="m-hero__left--p3">
                        <?php echo $hero_long_des; ?>
                         </p>
                    <button onclick="window.location.href='#'"> Learn more </button>
                </div>
                <?php if( !empty($hero_feature_image) ) : ?>
                <div class="m-hero__right" style="background-image: url(<?php echo $hero_feature_image['url'] ?> ) ;">
                <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="m-hero" id="about-us">
                <div class="m-hero__left">
                    <!-- <p class="m-hero__left--p1"> 02 <span> / 04 </span> </p> -->
                    <p class="m-hero__left--p2"> <?php echo $hero_short_description; ?> <?php bloginfo('name'); ?> </>
                    <h1> Reaching Our Goals</h1>
                    <p class="m-hero__left--p3">
                        <?php echo $hero_long_des; ?>
                    </p>
                    <button onclick="window.location.href='#'"> Learn more </button>
                </div>
                <?php if( !empty($hero_feature_image1) ) : ?>
                <div class="m-hero__right" style="background-image: url(<?php echo $hero_feature_image1['url'] ?> ) ;">
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="m-hero" id="about-us">
                <div class="m-hero__left">
                    <!-- <p class="m-hero__left--p1"> 02 <span> / 04 </span> </p> -->
                    <p class="m-hero__left--p2"> <?php echo $hero_short_description; ?> <?php bloginfo('name'); ?> </>
                    <h1> Reaching Our Goals</h1>
                    <p class="m-hero__left--p3">
                        <?php echo $hero_long_des; ?>
                    </p>
                    <button onclick="window.location.href='#'"> Learn more </button>
                </div>
                <?php if( !empty($hero_feature_image2) ) : ?>
                <div class="m-hero__right" style="background-image: url(<?php echo $hero_feature_image2['url'] ?> ) ;">
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="m-hero" id="about-us">
                <div class="m-hero__left">
                    <!-- <p class="m-hero__left--p1"> 02 <span> / 04 </span> </p> -->
                    <p class="m-hero__left--p2"> <?php echo $hero_short_description; ?> <?php bloginfo('name'); ?> </>
                    <h1> Reaching Our Goals</h1>
                    <p class="m-hero__left--p3">
                        <?php echo $hero_long_des; ?>
                    </p>
                    <button onclick="window.location.href='#'"> Learn more </button>
                </div>
                <?php if( !empty($hero_feature_image3) ) : ?>
                <div class="m-hero__right" style="background-image: url(<?php echo $hero_feature_image3['url'] ?> ) ;">
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div id="fraction"> </div>
    <div class="swiper-pagination"></div>
    <div class="swiper-button-prev"> &larr; </div>
    <div id="fraction1"> </div>
    <div class="swiper-button-next"> &rarr; </div>
</div>

<?php require_once(dirname(__FILE__) . '/index.php'); ?>