<!--isto kao i u prethodnom fajlu, probaj da nazoves tako sekcije da znas koju celinu obuhvataju-->
<?php
$panel_section_title = get_field('panel_section_title');
$panel_button_text = get_field('panel_button_text');
$panel_image_1 = get_field('panel_image_1');
$panel_image_2 = get_field('panel_image_2');
$panel_image_3 = get_field('panel_image_3');
$panel_image_4 = get_field('panel_image_4');
$panel_image_5 = get_field('panel_image_5');
$panel_image_6 = get_field('panel_image_6');
$panel_image_7 = get_field('panel_image_7');
$panel_image_8 = get_field('panel_image_8');
?>
<div class="m-panel" id="past-activities">
    <div class="m-panel__left">

        <?php if( !empty($panel_image_1) ) : ?>
        <div class="m-panel__img1" style="background-image: url(<?php echo $panel_image_1['url'] ?> ) ;">
            <?php endif; ?>
        </div>

        <?php if( !empty($panel_image_2) ) : ?>
        <div class="m-panel__img2" style="background-image: url(<?php echo $panel_image_2['url'] ?> ) ;">
        <?php endif; ?>
        </div>

        <?php if( !empty($panel_image_3) ) : ?>
        <div class="m-panel__img3" style="background-image: url(<?php echo $panel_image_3['url'] ?> ) ;">
            <?php endif; ?>
        </div>

        <?php if( !empty($panel_image_4) ) : ?>
        <div class="m-panel__img4" style="background-image: url(<?php echo $panel_image_4['url'] ?> ) ;">
            <?php endif; ?>
        </div>

    </div>
    <div class="m-panel__center">
            <h3> <?php echo $panel_section_title ?></h3>
            <button onclick="window.location.href='#'">
                <p> <?php echo $panel_button_text ?> </p> <i class="fas fa-chevron-right"></i>
            </button>
    </div>
    <div class="m-panel__right">
        <div>
            <?php if( !empty($panel_image_5) ) : ?>
            <div class="m-panel__img5" style="background-image: url(<?php echo $panel_image_5['url'] ?> ) ;">
                <?php endif; ?>
            </div>
            <?php if( !empty($panel_image_6) ) : ?>
            <div class="m-panel__img6" style="background-image: url(<?php echo $panel_image_6['url'] ?> ) ;">
                <?php endif; ?>
            </div>

        </div>
        <?php if( !empty($panel_image_7) ) : ?>
        <div class="m-panel__img7" style="background-image: url(<?php echo $panel_image_7['url'] ?> ) ;">
            <?php endif; ?>
        </div>

        <?php if( !empty($panel_image_8) ) : ?>
        <div class="m-panel__img8" style="background-image: url(<?php echo $panel_image_8['url'] ?> ) ;">
            <?php endif; ?>
        </div>
    </div>
</div>

<?php require_once(dirname(__FILE__) . '/index.php'); ?>