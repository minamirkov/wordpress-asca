<!--da se ne ponavljam :D proci kroz sve fajlove i ispraviti prethodno navedeno u komentarima-->
<?php
$video_box_feature_image     =   get_field('video-box_feature_image');
$video_box_section_title     =   get_field('video-box_section_title');
$video_box_section_description =   get_field('video-box_section_description');
$video_box_section_text = get_field('video-box_section_text');
$video_box_button = get_field('video-box_button_text');
?>
<div class="m-video-box">
    <?php if( !empty($video_box_feature_image) ) : ?>
    <div class="m-video-box__left" style="background-image: url(<?php echo $video_box_feature_image['url'] ?> ) ;">
        <?php endif; ?>
    </div>
    <div class="m-video-box__right">
        <span> &nbsp 2:24 &nbsp </span>
        <h6> <?php echo $video_box_section_title; ?> </h6>
        <p class="m-video-box__right--headings"> <?php echo $video_box_section_description; ?> </p>
        <p class="m-video-box__right--paragraph">  <?php echo $video_box_section_text?>
        </p>
        <button onclick="window.location.href='#'"> <i class="fas fa-play"></i>
        <?php echo $video_box_button ?>
        </button>
    </div>
</div>


<?php require_once(dirname(__FILE__) . '/index.php'); ?>