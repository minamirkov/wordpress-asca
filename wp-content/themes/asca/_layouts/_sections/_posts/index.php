<!--isto kao i u prethodnom fajlu, probaj da nazoves tako sekcije da znas koju celinu obuhvataju-->
<!--mozes probati da koristis _wr i _w i kolone-->
<div class="m-posts" id="news">
    <div class="_wr">
        <div class="_w">
            <div class="_xs12 _s12 _m12 _l6 _xl4 m-posts__left">
                    <?php
                    $the_query = new WP_Query( 'posts_per_page=1' ); ?>
                    <?php
                    while ($the_query -> have_posts()) : $the_query -> the_post();
                    ?>
                        <p><?php echo get_the_date("d.m.Y."); ?> </p>
                        <h4><a href="<?php the_permalink() ?>"><?php the_title(); ?></a> </h4>


                            <a href="<?php the_permalink() ?>"> <i class="fas fa-long-arrow-alt-right"></i> </a>
                        <?php if ( has_post_thumbnail() ) :
                        $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'medium' ); ?>
                            <div class="_xs12 _s12 _m12 _l6 _xl3 m-posts__left--img">
                                <a href="<?php the_permalink(); ?>"><img class="m-posts__img"src="<?php echo $featured_image[0]; ?>" alt='' /></a>
                            </div>
                    <?php endif; ?>
                    <?php endwhile;
                    wp_reset_postdata();
                    ?>
            </div>
            <div class="_xs12 _s12 _m12 _l12 _xl5 m-posts__right ">
                <div class="_w">
                        <?php $the_query = new WP_Query( 'posts_per_page=4' ); ?>
                        <?php
                        while ($the_query -> have_posts()) : $the_query -> the_post();
                            ?>
                        <div class="m-posts__right-post">
                            <p><?php echo get_the_date("d.m.Y."); ?> </p>
                            <h5><a href="<?php the_permalink() ?>"><?php the_title(); ?></a> </h5>
                            <a href="<?php the_permalink() ?>"> <i class="fas fa-long-arrow-alt-right"></i> </a>
                        </div>
                        <?php
                        endwhile;
                        wp_reset_postdata();
                        ?>
                </div>

            </div>
        </div>
    </div>
</div>


<?php require_once(dirname(__FILE__) . '/index.php'); ?>