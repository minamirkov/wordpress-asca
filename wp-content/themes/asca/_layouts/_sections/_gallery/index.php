<!--isto kao i u prethodnom fajlu, probaj da nazoves tako sekcije da znas koju celinu obuhvataju-->
<?php
$gallery_first_title = get_field('gallery_first_title');
$gallery_second_title = get_field('gallery_second_title');
$gallery_third_title = get_field('gallery_third_title');
// Slide 1 //
$slide_1_image = get_field('slide_1_image');
$slide_1_button_text = get_field('slide_1_button_text');
$slide_2_image = get_field('slide_2_image');
$slide_2_button_text = get_field('slide_2_button_text');
$slide_3_image = get_field('slide_3_image');
$slide_3_button_text = get_field('slide_3_button_text');
$slide_4_image = get_field('slide_4_image');
$slide_4_button_text = get_field('slide_4_button_text');
$slide_5_image = get_field('slide_5_image');
$slide_5_button_text = get_field('slide_5_button_text');
$slide_6_image = get_field('slide_6_image');
$slide_6_button_text = get_field('slide_6_button_text');
?>
<div class="m-gallery" id="history">
    <div class="m-gallery__top">
        <p class="m-gallery__top--apart"> <?php echo $gallery_first_title; ?> </p>
        <h2> <?php echo $gallery_second_title; ?>  </h2>
        <p class="m-gallery__top--tibet"> <?php echo $gallery_third_title; ?>  </p>
    </div>
    <div class="swiper gallerySwiper">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <?php if( !empty($slide_1_image) ) : ?>
                <img src="<?php echo $slide_1_image['url'];?>" alt="">
                <?php endif; ?>
                <button onclick="window.location.href='#'"> <?php echo $slide_1_button_text; ?> <br> <span> (8) Events </span> </button>
            </div>
            <div class="swiper-slide">
                <?php if( !empty($slide_2_image) ) : ?>
                    <img src="<?php echo $slide_2_image['url'];?>" alt="">
                <?php endif; ?>
                <button onclick="window.location.href='#'"> <?php echo $slide_2_button_text; ?>  <br> <span> (3) Events </span></button>
            </div>
            <div class="swiper-slide">
                <?php if( !empty($slide_3_image) ) : ?>
                    <img src="<?php echo $slide_3_image['url'];?>" alt="">
                <?php endif; ?>
                <button onclick="window.location.href='#'">  <?php echo $slide_3_button_text; ?> <br> <span> (12) Events </span> </button>
            </div>
            <div class="swiper-slide">
                <?php if( !empty($slide_4_image) ) : ?>
                    <img src="<?php echo $slide_4_image['url'];?>" alt="">
                <?php endif; ?>
                <button onclick="window.location.href='#'"> <?php echo $slide_4_button_text; ?> <br> <span> (9) Events </span> </button>
            </div>
            <div class="swiper-slide">
                <?php if( !empty($slide_5_image) ) : ?>
                    <img src="<?php echo $slide_5_image['url'];?>" alt="">
                <?php endif; ?>
                <button onclick="window.location.href='#'"> <?php echo $slide_5_button_text; ?><br> <span> (8) Events </span> </button>
            </div>
            <div class="swiper-slide">
                <?php if( !empty($slide_6_image) ) : ?>
                    <img src="<?php echo $slide_6_image['url'];?>" alt="">
                <?php endif; ?>
                <button onclick="window.location.href='#'"> <?php echo $slide_6_button_text; ?> <br> <span> (3) Events </span></button>
            </div>
        </div>
        <div class="swiper-button-prev"> &larr; </div>
        <div class="swiper-button-next"> &rarr; </div>
        <div class="swiper-pagination"></div>
    </div>
</div>

<?php require_once(dirname(__FILE__) . '/index.php'); ?>