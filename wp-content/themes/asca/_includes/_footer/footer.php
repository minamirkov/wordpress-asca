<?php
$hero_section_title     =   get_field('hero_section_title');
$hero_long_des =   get_field('hero_long_description');
$slide_1_button_text = get_field('slide_1_button_text');
$slide_2_button_text = get_field('slide_2_button_text');
$slide_3_button_text = get_field('slide_3_button_text');
$slide_4_button_text = get_field('slide_4_button_text');
$slide_5_button_text = get_field('slide_5_button_text');
$slide_6_button_text = get_field('slide_6_button_text');
$about_us_button_text = get_field('about_us_button_text');
$phone = get_field('phone');
$email = get_field('email');
$adress = get_field('adress');
?>

    <footer>
    <div class="m-footer" id="contact">
        <div class="_wr">
            <div class="_w">
                <div class="_xs12 _s12 _m6 _l3 _3">
                    <p class="m-footer__headings"> ABOUT US </p>
                    <p class="pink"> <?php echo $hero_section_title; ?> </p>
                    <p class="m-footer__paragraph"> <?php echo $hero_long_des; ?> </p>
                    <button> <?php echo $about_us_button_text; ?>
                        <hr> </button>
                </div>
                <div class="_xs12 _s12 _m6 _l3 _3">
                    <p class="m-footer__headings"> EVENTS </p>
                    <div class="column">
                        <div>
                            <p> <?php echo $slide_1_button_text; ?> </p>
                            <p> <?php echo $slide_2_button_text; ?> </p>
                            <p> <?php echo $slide_3_button_text; ?> </p>
                            <p> <?php echo $slide_4_button_text; ?> </p>
                        </div>
                        <div>
                            <p> <?php echo $slide_5_button_text; ?> </p>
                            <p> <?php echo $slide_6_button_text; ?> </p>
                        </div>
                    </div>
                </div>
                <div class="_xs12 _s12 _m6 _l3 _3">
                    <p class="m-footer__headings" > LATEST NEWS </p>
                    <?php $the_query = new WP_Query( 'posts_per_page=1' ); ?>
                    <?php
                    while ($the_query -> have_posts()) : $the_query -> the_post();
                        ?>
                        <div>
                        <?php if ( has_post_thumbnail() ) :
                            $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'medium' ); ?>
                            <div class="m-footer__img">
                                <a href="<?php the_permalink(); ?>"><img class="img" src="<?php echo $featured_image[0]; ?>" alt='' /></a>
                            </div>
                        <?php endif; ?>
                            <p class="pink"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a> </p>
                            <a href="<?php the_permalink() ?>">
                                <button> Read more
                                    <hr> </button>
                            </a>
                        </div>
                    <?php endwhile;
                    wp_reset_postdata();
                    ?>
                </div>
                <div class="_xs12 _s12 _m6 _l3 _3">
                    <p class="m-footer__headings"> CONTACT </p>
                    <p> Phone &emsp;<span>/</span>&emsp; <?php echo $phone; ?> </p>
                    <p> Email &emsp;<span>/</span>&emsp; <?php echo $email; ?> </p>
                    <p> Adress &emsp;<span>/</span>&emsp; <?php echo $adress; ?></p>

                    <p class="m-footer__headings social"> SOCIAL MEDIA </p>
                    <div class="icons">
                        <a href="#"> <img src="<?php bloginfo('template_directory');?>/src/images/_icons/facebook-logo-button@2x.png" alt="facebook"> </a>
                        <a href="#"> <img src="<?php bloginfo('template_directory');?>/src/images/_icons/twitter-logo-button@2x.png" alt="twitter"> </a>
                        <a href="#"> <img src="<?php bloginfo('template_directory');?>/src/images/_icons/linkedin-logo-button@2x.png" alt="linkedin"> </a>
                        <a href="#"> <img src="<?php bloginfo('template_directory');?>/src/images/_icons/instagram-logo@2x.png" alt="instagram"> </a> <br>
                        <a href="#"> <img src="<?php bloginfo('template_directory');?>/src/images/_icons/pinterest-logo-button@2x.png" alt="pinterest"> </a>
                        <a href="#"> <img src="<?php bloginfo('template_directory');?>/src/images/_icons/Slack@2x.png" alt="slack"> </a>
                        <a href="#"> <img src="<?php bloginfo('template_directory');?>/src/images/_icons/Youtube@2x.png" alt="youtube"> </a>
                    </div>
                </div>
                <div class="_wr">
                    <p class="m-footer__headings"> NAVIGATION </p>
                    <div class="m-footer__nav">
                        <button onclick="window.location.href='#'"> About Us <hr> </button>
                        <button onclick="window.location.href='#history'"> A Wealth of History <hr> </button>
                        <button onclick="window.location.href='#past-activities'"> Past activities <hr></button>
                        <button onclick="window.location.href='#news'"> News <hr> </button>
                        <button onclick="window.location.href='#contact'"> Contact <hr></button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</footer>
<?php wp_footer(); ?>
