<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Dosth
 */
?>
<?php require_once(dirname(__FILE__) . '/_layouts/_partials/nav.php'); ?>
<section class="m-single">
    <div class="m-single__post">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <h4> <?php the_title(); ?> </h4>
    <p class="m-single__post-date"> <?php echo get_the_date(); ?> </p>
    <p> <?php the_content(); ?> </p>
<div style="padding-left: 20px;">
<?php
if ( comments_open() || get_comments_number() ) :
    comments_template();
endif;
    ?>
</div>
<?php endwhile; ?>
<?php endif; ?>
    </div>
    <div class="m-single__sidebar"> <?php get_sidebar(); ?> </div>
</section>
<?php get_header(); get_footer(); ?>

