<?php
/**
 * Enqueue scripts and styles.
 */
function asca_scripts()
{
    // Note, the is_IE global variable is defined by WordPress and is used
    // to detect if the current browser is internet explorer.
    $rand = rand(1, 99999999999);
    wp_enqueue_style('asca-style', get_stylesheet_uri(), '', $rand);

    wp_enqueue_style('material', 'https://fonts.googleapis.com/icon?family=Material+Icons', '');


    wp_enqueue_style('icon', get_template_directory_uri(). '/bundles/images/_icons/fav.ico', '');
    wp_enqueue_script('fontawesome', 'https://kit.fontawesome.com/a0006c21b2.js', '');

    wp_enqueue_script('swiper', get_template_directory_uri() . '/bundles/scripts/swiper-bundle.min.js', '');
    wp_enqueue_script('script', get_template_directory_uri() . '/src/scripts/src/script.js', '');


}
add_action( 'wp_enqueue_scripts', 'asca_scripts' );
