<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 */

?>
<?php require_once(dirname(__FILE__) . '/_layouts/_partials/nav.php'); ?>


    <div id="primary" class="content-area m-single">
        <div id="content" class="site-content m-single__post" role="main">

            <header class="page-header">
                <h1 class="page-title"><?php _e( 'Not Found'); ?></h1>
            </header>

            <div class="page-content">
                <p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?'); ?></p>

                <?php get_search_form(); ?>
                <div class="m-single__sidebar">
                    <?php
                    get_sidebar( 'content' );
                    get_sidebar(); ?>
                </div>

            </div><!-- .page-content -->

        </div><!-- #content -->
    </div><!-- #primary -->



<?php
get_header();
get_footer();
